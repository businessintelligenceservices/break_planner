from workalendar.registry import registry
import polars as pl
regions = registry.get_calendars(include_subregions = True)
df_regions = pl.Series(regions.keys()).to_frame()
df_regions.write_csv(r".\holiday_regions.csv")

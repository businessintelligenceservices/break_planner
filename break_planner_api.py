# Flask web app on eu.pythonanywhere.com
from flask import Flask, request
from datetime import date
from workalendar.registry import registry
import polars as pl


app = Flask(__name__)

@app.route("/", methods = ["GET", "POST"])
def handle_request():

    var_region = request.args.get("region")
    date_this_year = date.today().year
    date_next_year = date.today().year + 1 # Needs this...

    CalendarClass = registry.get(var_region)

    calendar = CalendarClass()

    # Init dataframe
    df_holidays = pl.DataFrame(schema = {"HOLIDAY_DATE": pl.Date, "HOLIDAY": pl.Utf8})

    for year_i in range(date_this_year, date_next_year + 1): # ... and this
        df_holidays = df_holidays.extend(pl.DataFrame(calendar.holidays(year_i), schema = ["HOLIDAY_DATE", "HOLIDAY"]))

    return(df_holidays.write_csv())
